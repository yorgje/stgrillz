//-----utils-----
Array.prototype.diff = function(a) 
{
    return this.filter(function(i) 
	{
		return !(a.indexOf(i) > -1);
	});
};

String.prototype.arg = function(key, val) 
{
	return this
		.replace(new RegExp('{{\\s*' + key + '\\s*}}', 'ig'), val) //unescaped
		.replace(new RegExp('{{\\s*' + key + '\\s*\\s*\\|\\s*e\\s*}}', 'ig'), val.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;')) //escaped
	;						// ^^^^^ \-overload...
};

function getCookie(c_name) //I have no idea wtf is this, ripped from w3c site
{
	var c_value = document.cookie;
	var c_start = c_value.indexOf(" " + c_name + "=");
	if (c_start == -1)
	{
		c_start = c_value.indexOf(c_name + "=");
	}
	if (c_start == -1)
	{
		c_value = '';
	}
	else
	{
		c_start = c_value.indexOf("=", c_start) + 1;
		var c_end = c_value.indexOf(";", c_start);
		if (c_end == -1)
		{
			c_end = c_value.length;
		}
		c_value = unescape(c_value.substring(c_start,c_end));
	}
	return c_value;
}

//prevent sending few request at once 
(function($,sr){
  jQuery.fn[sr] = function(fn){  return fn ? this.bind('click', debounce(fn, 1000, true)) : this.trigger(sr); };
})(jQuery,'slowclick'); 

//-----the meat-----
function refresh()
{	
	//get list
	$.ajax({
		url: "ajax/list.json", 
		dataType: "json"
	})
	.done(listReady);
}

function listReady(data)
{
	var twitch_loaded = 
	$(".twitch.stream").map(function()
	{
		return this.id;
	}).get();

	var hiddenTwitchers = getCookie('hiddenTwitchers').split(' ');
	
	//omit already loaded and hidden
	list = data.twitchers.diff(twitch_loaded).diff(hiddenTwitchers);

	//get twitch data
	$.getJSON("http://api.justin.tv/api/stream/list.json?channel=" + list.join() + "&jsonp=?", twitchReady);
	
	//get hash data
	//TODO
}

function twitchReady(data)
{
	//get twitch embed codes
	if(data.length > 0)
	{
		var video_code =
			'<object type="application/x-shockwave-flash" \
					height="378" \
					width="620" \
					class="video" \
					data="http://www.twitch.tv/widgets/live_embed_player.swf?channel={{name}}" \
					bgcolor="#000000"> \
			  <param  name="allowFullScreen" \
					  value="true" /> \
			  <param  name="allowScriptAccess" \
					  value="always" /> \
			  <param  name="allowNetworking" \
					  value="all" /> \
			  <param  name="movie" \
					  value="http://www.twitch.tv/widgets/live_embed_player.swf" /> \
			  <param  name="flashvars" \
					  value="hostname=www.twitch.tv&channel={{name}}&auto_play=false&start_volume=50&initCallback=onPlayerLoad" /> \
			</object> ';
		var chat_url = 'http://twitch.tv/chat/embed?channel={{name}}&popout_chat=true'
		var channel_url = 'http://www.twitch.tv/{{name}}'
		
		var rv = 
		$.map(data, function(val) 
		{
			var login = val.channel.login;
			codes = 
			{
				name : login,
				status : val.channel.status,
				metagame : val.channel.meta_game || "no specified game",
				image : val.channel.image_url_medium,
				site : "twitch",
				video : video_code.arg('name', login), 
				chat : chat_url.arg('name', login),
				channel : channel_url.arg('name', login)
			}
			return codes;
		});
		
		getTemplate(rv);
	}
}

function hashReady(data)
{
	//TODO
	getTemplate(rv);
}

function getTemplate(rv)
{
	$.ajax({
		url: "ajax/stream.template", 
		dataType: "text"
	})
	.done(function(template)
	{
		embedStream(rv, template);
	});
}

function embedStream(rv, template)
{
	$.each(rv, function(ind, val) 
	{
		if($('#' + val.name).length)//double check for duplicates
			return;
		$('#main').append(
			template
			.arg('name', val.name)
			.arg('status', val.status)
			.arg('game', val.metagame)
			.arg('image', val.image)
			.arg('site', val.site)
			.arg('video', val.video)
			.arg('chat', val.chat)
			.arg('channel', val.channel)
		);
	});
}

var interval = null;

function toggleRefresh(event)
{
	if(interval == null)
	{
		interval = setInterval(refresh, 60*1000);
		$('.toggleRefresh').html("&#x2713; Auto");
	}
	else
	{
		clearInterval(interval);
		interval = null;
		$('.toggleRefresh').html("&#x2717; Auto");
	}
}

$(document).ready(function()
{
	$('.refresh').slowclick(refresh);
	$('.toggleRefresh').click(toggleRefresh);
	refresh()
	
	interval = setInterval(refresh, 60*1000);//twitch caches their responses for 60 sec, so no point in calling it more frequently
});
