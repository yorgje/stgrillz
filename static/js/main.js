//-----utils-----

function zeroIfNaN(arg)
{
	var res = (isNaN(res = parseInt(arg)) ? 0 : res);
	return res;
}

function moveViewPort(arg)
{
	$('html, body').scrollTop(arg.offset().top);
}

// debouncing function from John Hann
// http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
function debounce(func, threshold, execAsap) {
	var timeout;

	return function debounced () {
	  var obj = this, args = arguments;
	  function delayed () {
		  if (!execAsap)
			  func.apply(obj, args);
		  timeout = null;
	  };

	  if (timeout)
		  clearTimeout(timeout);
	  else if (execAsap)
		  func.apply(obj, args);

	  timeout = setTimeout(delayed, threshold || 100);
	};
}
(function($,sr)
{
	jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn, 500)) : this.trigger(sr); };
})(jQuery,'smartresize');

//-----resize functions-----

var defualtVideoWidth = 620;
var defualtVideoHeight = 378;

var barHeight = 28;//height of navigation bar in player
var ratio = parseFloat(defualtVideoHeight - barHeight) / defualtVideoWidth; //screen ratio

function resize(par)
{
	par.each(function() //composite
	{ 
		var video = $(this).find('.video');
		var chat = $(this).find('.chat');
		var col = $(this).find('.col');
		
		var header = $(this).find("header");
		var buttons = $(this).find('.buttons');
		
		var chatWidth = chat.hasClass('hidden') ? 0 : chat.outerWidth(true);

		if($(this).hasClass('fixedSize'))
		{
			if(video.width() == defualtVideoWidth)//size is already default, let's save on one click
			{
				$(this).removeClass('fixedSize');
				$(this).find('.resizeStream').text('Size: default');
			}
			/*
			video.height(defualtVideoHeight*3/2);
			video.width(defualtVideoWidth*3/2);
			col.width(defualtVideoWidth*3/2);
			//*/
		}
		else
		{
			var width = $(window).width() - ( 
					chatWidth + 
					zeroIfNaN(col.css('margin-left')) + 
					zeroIfNaN(col.css('margin-right'))
				); 
			var height = Math.floor(width*ratio + barHeight);
			
			var maxHeight = $(window).height() - (
					header.outerHeight(true) + 
					zeroIfNaN(video.css('margin-top')) +
					zeroIfNaN(video.css('margin-bottom')) +
					buttons.outerHeight(true) +
					zeroIfNaN(col.css('margin-top')) +
					zeroIfNaN(col.css('margin-bottom'))
				);
			
			if(height > maxHeight) //scale down if heigth is too high
			{
				height = maxHeight;
				width = Math.floor((height - barHeight)/ratio);
			}

			if($(this).hasClass('fitToWindow') && !(height < defualtVideoHeight || width < defualtVideoWidth))//fit to window size
			{ 		
				video.height(height);
				video.width(width);
				col.width(width);
			}
			else //defualt
			{
				video.height(defualtVideoHeight);
				video.width(defualtVideoWidth);
				col.width(defualtVideoWidth);
			}
		}
		$(this).width(col.outerWidth(true) + chatWidth);
		
		chat.height(col.height());
	});
}

function resizeAll(event)
{
	$('.stream').each(function() 
	{
		if($(this).hasClass('fitToWindow')) //check if resize is needed
			resize($(this));
	});
}

function resizeStream(event)
{
	var button = $(event.target);
	var par = button.parents('.stream');
	//*
	if(par.hasClass('fitToWindow')) //fixed
	{
		par.removeClass('fitToWindow');
		par.addClass('fixedSize');
		button.text('Size: fixed');
	}
	else if(par.hasClass('fixedSize')) //default
	{
		par.removeClass('fixedSize');
		button.text('Size: default');
	}
	else //fit
	{
		par.addClass('fitToWindow');
		button.text('Size: fit to window');
	}
	/*/
	if(par.hasClass('fixedSize'))
	{
		par.removeClass('fixedSize');
		par.addClass('fitToWindow');
		button.text('Default size');
	}
	else if(par.hasClass('fitToWindow'))
	{
		par.removeClass('fitToWindow');
		button.text('Resize');
	}
	else
	{
		par.addClass('fixedSize');
		button.text('Fit to window');
	}//*/

	resize(par);
	
	if(par.hasClass('fitToWindow'))
		moveViewPort(par);
}


//-----Stream hide/show functions-----
function hide(par)
{
	par.find('.content').addClass('hidden');
	par.find('.placeholder').removeClass('hidden');
	
	var chat = par.find('.chat');
	if(!chat.hasClass('hidden'))
		disconnectChat(chat);
	
	//watch out composite!!!
	//>>shouldn't<< matter because we only want to fetch style
	var col = par.find('.col');
	par.width(defualtVideoWidth + 
		zeroIfNaN(col.css('margin-left')) + 
		zeroIfNaN(col.css('margin-right'))
	);
}

function show(par)
{
	par.find('.content').removeClass('hidden');
	par.find('.placeholder').addClass('hidden');
	
	var chat = par.find('.chat');
	if(!chat.hasClass('hidden'))
		connectChat(chat);
	
	resize(par);
}

function hideSelf(event)
{
	hide($(event.target).parents('.stream'));
}

function showSelf(event) 
{
	var par = $(event.target).parents('.stream');
	show(par);
	moveViewPort(par);
}

function hideOthers(event)
{
	var par = $(event.target).parents('.stream');
	var oth = $('.stream').not(par);
	hide(oth);
	moveViewPort(par);
}

function hideAll(event)
{
	hide($(".stream"));
}

function unhideAll(event)
{
	show($(".stream"));
}

function remove(event)
{
	var par = $(event.target).parents('.stream');
	
	par.remove();
}

//-----Chat funtions-----
function disconnectChat(chat) //actually - set timeout for disconnect
{
	chat.each(function() //composite
	{ 
		var c = $(this);
		var timeout = setTimeout(function()
		{
			c.attr('src', '');
		}, 2*60*1000); //after 2 min	
		c.data('timeout', timeout); //store timeout handle
	});
}

function connectChat(chat) //if needed, also - clear timeout
{
	chat.each(function() //composite
	{ 
		var timeout = $(this).data('timeout');
		if(timeout !== undefined) 
			clearTimeout(timeout);
		
		if($(this).attr('src') == '')//load if needed
			$(this).attr('src', $(this).data('url')); //get url	
	});
}

function hideChat(par)
{
	var button = par.find('.toggleChat');
	var chat = par.find('.chat');	

	button.text('Show chat');

	disconnectChat(chat);

	chat.addClass('hidden');
	resize(par);
}

function showChat(par)
{
	var button = par.find('.toggleChat');
	var chat = par.find('.chat');	
	
	button.text('Hide chat');
	
	connectChat(chat);
	
	chat.removeClass('hidden');
	resize(par);
}

function toggleChat(event)
{
	var par = $(event.target).parents('.stream');
	
	if(par.find('.chat').hasClass('hidden'))
		showChat(par);
	else
		hideChat(par);
}

function popupChat(event)
{
	var url = $(event.target).parents('.stream').find('.chat').data('url');

	window.open(url, '', 'scrolling=no,width=350,height=500');
	return false; //prevent default/propagation
}

function hideAllChats(event)
{
	hideChat($(".stream"));
}

function showAllChats(event)
{
	showChat($(".stream"));
}

//bind events
$(document).ready(function()
{
	$('.hideAll').click(hideAll);
	$('.unhideAll').click(unhideAll);
	$('.hideAllChats').click(hideAllChats);
	$('.showAllChats').click(showAllChats);
	
	/*
	$('.hideSelf').click(hideSelf);
	$('.showSelf').click(showSelf);
	$('.hideOthers').click(hideOthers);
	$('.resizeStream').click(resizeStream);
	$('.popupChat').click(popupChat);
	$('.toggleChat').click(toggleChat);
	$('.remove').click(remove);
	*/
	$(document).on('click', '.hideSelf', hideSelf);
	$(document).on('click', '.showSelf', showSelf);
	$(document).on('click', '.hideOthers', hideOthers);
	$(document).on('click', '.resizeStream', resizeStream);
	$(document).on('click', '.popupChat', popupChat);
	$(document).on('click', '.toggleChat', toggleChat);
	$(document).on('click', '.remove', remove);

	//$(window).resize(resizeAll);
	$(window).smartresize(resizeAll);
	
});

//twitch dumbness
//var once = false;
onPlayerLoad = function () 
{
	//if(!once)
	//{
	//	once = true;
		$(".video").each(function()
		{
			$(this)[0].pauseVideo();
		});
	//}
};