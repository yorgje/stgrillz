from models import Twitcher, Hasher, Hitboxer
import webapp2 as w
import os
import urllib2
import json
import httplib
from random import shuffle
from random import randint
from google.appengine.ext import db
from google.appengine.api import memcache
from google.appengine.api import urlfetch
import threading
from Queue import Queue

INCL_TAGS = ['[stg]'
        , '[2hu]'
        , '[gay]'
        , '[ayy]'
        , '[420]'
        , '[t\xc5\x8dh\xc5\x8d]'
        , '[\xe6\x9d\xb1\xe6\x96\xb9]'
        ]
WHITELIST = ['stgweekly', 'kits2hu']

def LOGO():
    return randint(0,4)

def get_embed_codes():
    twitchData,hitboxData = None, None
    try:
        twitchData = getTwitchData_Multithread()
    except Exception as e: 
        memcache.set('last_error', e.__str__())
    try:
        hitboxData = getHitboxData_Multithread()
    except Exception as e: 
        memcache.set('last_error', e.__str__())
                    
    embed_codes=[]

    if twitchData is not None:
        embed_codes += getTwitchEmbedCodes(twitchData)
    if hitboxData is not None:
        embed_codes += getHitboxEmbedCodes(hitboxData)
                    
    shuffle(embed_codes)
    return embed_codes

def getTwitchersList():
    twitchers = memcache.get('twitchers')
    
    if twitchers is None: #if not in the cache, fetch it
        twitcherObj = db.GqlQuery("SELECT login FROM Twitcher")
        twitchers = []
        
        for obj in twitcherObj:
            twitchers.append(obj.login.lower())
        memcache.set('twitchers', twitchers) #and then add to cache
                    
    return twitchers
		
def getHitboxersList():
    hitboxers = memcache.get('hitboxers')
    
    if hitboxers is None or hitboxers == []: #if not in the cache, fetch it
        hitboxerObj = db.GqlQuery("SELECT login FROM Hitboxer")
        hitboxers = []
        
        for obj in hitboxerObj:
            hitboxers.append(obj.login.lower())
        memcache.set('hitboxers', hitboxers) #and then add to cache
                    
    return hitboxers
		
def cookieGetList(request, key):
    raw_cookie = request.cookies.get(key)
    val = []
    if raw_cookie is not None:
        val = raw_cookie.split(' ')
    return val
		
def cookieSetList(response, key, val):
    if val: #if not empty
        cookie = ' '.join(val) #twitch doesn't allow spaces in nicknames r-right?
        response.set_cookie(key, cookie, overwrite=True, max_age=2*10**9) #looong time
    else:
        response.delete_cookie(key)


def getDataMultithreader(request_urls, worker):
    stream_data_q = Queue()
    threads = []
    for url in request_urls:
        t = threading.Thread(target=worker, args=(url,stream_data_q))
        threads.append(t)
        t.start()
    for t in threads:
        t.join()
    ret = []
    while not stream_data_q.empty():
        data = stream_data_q.get()
        ret.append(data)
    return ret

def streamfetch_worker(url, doc2ret, q=None):
    try:
        request = urllib2.urlopen(url, timeout=5)
        doc = request.read()
        ret = doc2ret(doc)
    except urllib2.HTTPError:
        print url
        ret = doc2ret('')
    if q:
        if ret:
            q.put(ret)
    else:
        return ret

def twitch_streamfetcher(doc):
    try:
        ret= json.loads(doc)['streams']
    except:
        ret= []
    return ret

def hitbox_streamfetcher(doc):
    try:
        j = json.loads(doc)
        live=int(j['livestream'][0]['media_is_live'])
        if live:
            ret = j
        else:
            ret = None
    except:
        ret= None
    return ret

def getTwitchData():
    api_url = 'https://api.twitch.tv/kraken/streams?channel='
    f = lambda u: streamfetch_worker(u, twitch_streamfetcher)
    twitchers = getTwitchersList()
    res = []
    usernames = ""
    for twitcher in twitchers:
        usernames += twitcher + ","
        if len(usernames) > 1000:
            res += f(api_url + usernames)
            usernames = ""
    res += f(api_url + usernames)
    return res

def getTwitchData_Multithread():
    api_url = 'https://api.twitch.tv/kraken/streams?channel='
    twitchers = getTwitchersList()
    res = []
    usernames = ""
    request_urls = []
    for twitcher in twitchers:
        usernames += twitcher + ","
        if len(usernames) > 1000:
            request_urls.append(api_url + usernames)
            usernames = ''
    request_urls.append(api_url + usernames)

    worker = lambda u,q: streamfetch_worker(u, twitch_streamfetcher, q)
    stream_data = getDataMultithreader(request_urls,worker)
    for data in stream_data:
        res += data
    return res

def getHitboxData():
    hitboxers = getHitboxersList()
    res = []
    f = lambda u: streamfetch_worker(u, hitbox_streamfetcher)
    for h in hitboxers:
        url = 'http://api.hitbox.tv/media/live/' + h
        j = f(url)
        if j:
            res.append(j)
    return res

def getHitboxData_Multithread():
    hitboxers = getHitboxersList()
    res = []
    request_urls = []
    for h in hitboxers:
        request_urls.append('http://api.hitbox.tv/media/live/' + h)
    worker = lambda u,q: streamfetch_worker(u, hitbox_streamfetcher, q)
    stream_data = getDataMultithreader(request_urls, worker)
    for data in stream_data:
        if data:
            res.append(data)
    return res

def getTwitchEmbedCodes(data):
    '''
    rv[0] are embed_enabled=False, rv[1] are embed_enabled=True
    '''
    rv = []
    video_code='''
                    <object type="application/x-shockwave-flash" 
                                                    height="378" 
                                                    width="620" 
                                                    class="video" 
                                                    data="http://www.twitch.tv/widgets/live_embed_player.swf?channel=%(name)s" 
                                                    bgcolor="#000000">
                            <param	name="allowFullScreen" 
                                                            value="true" />
                            <param	name="allowScriptAccess" 
                                                            value="always" />
                            <param	name="allowNetworking" 
                                                            value="all" />
                            <param	name="movie" 
                                                            value="http://www.twitch.tv/widgets/live_embed_player.swf" />
                            <param	name="flashvars" 
                                                            value="hostname=www.twitch.tv&channel=%(name)s&auto_play=false&start_volume=50" />
                    </object>
    '''
    chat_url='''http://twitch.tv/chat/embed?channel=%(name)s&popout_chat=true'''
    channel_url='''http://www.twitch.tv/%(name)s'''
    if data:
        for stream in data:
            site="twitch"
            if 'game' in stream.keys():
                metagame = stream['game']
            else:
                metagame="no specified game"
            stream = stream['channel']
            name = stream['name']
            try:
                status = stream['status']
            except:
                status = ''
            image = stream['logo']
            #image = "http://www-cdn.jtvnw.net/images/xarth/404_user_50x50.png"
            if [tag for tag in INCL_TAGS if tag in status.encode('utf-8').lower()] or name.encode('utf-8') in WHITELIST:
                rv.append((name,
                            status,
                            metagame,
                            image,
                            site,
                            video_code % {'name': name}, 
                            chat_url % {'name': name},
                            channel_url % {'name': name}))
        shuffle(rv)
    return rv


def getHitboxEmbedCodes(data):
    rv = []
    video_code='''
            <iframe
                    width="620" 
                    height="378" 
                    src="http://hitbox.tv/#!/embed/%(name)s" 
                    frameborder="0" 
                    allowfullscreen>
            </iframe>
    '''
    chat_url='''
            http://www.hitbox.tv/embedchat/%(name)s
            '''
    channel_url='''http://www.hitbox.tv/%(name)s'''
    if data:
        for stream in data:
            site="hitbox"
            stream = stream['livestream'][0]
            if stream['category_name']:
                metagame = stream['category_name']
            else:
                metagame="no specified game"
            name = stream['media_name']
            status = stream['media_status']
            image = 'http://edge.vie.hitbox.tv'+stream['channel']['user_logo']
            if [tag for tag in INCL_TAGS if tag in status.encode('utf-8').lower()] or name in WHITELIST:
                rv.append((name,
                            status,
                            metagame,
                            image,
                            site,
                            video_code % {'name': name}, 
                            chat_url % {'name': name},
                            channel_url % {'name': name}))
        shuffle(rv)
    return rv
