from google.appengine.ext import db

class Twitcher(db.Model):
    '''Models an individual Twitcher in TwitchStreamers with their login'''
    login = db.StringProperty()
class Hasher(db.Model):
    '''Models an individual Hasher in HashStreamers with their login'''
    login = db.StringProperty()
class Hitboxer(db.Model):
    login = db.StringProperty()
