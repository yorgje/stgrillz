from utils import *
from models import Twitcher,  Hitboxer
import jinja2


TEMPLATE_DIR=os.path.join(os.path.dirname(__file__), 'templates')
JINJA_ENVIRONMENT = jinja2.Environment(
		loader=jinja2.FileSystemLoader(TEMPLATE_DIR))

def get_strmpage_html(template_filename):
    embed_codes = get_embed_codes()
    template_values = {
                    'embed_codes': embed_codes,
                    'logo':LOGO()
                    }
    template = JINJA_ENVIRONMENT.get_template(template_filename)
    return template.render(template_values)

def get_logopage_html(template_filename):
    ''' generates the html for a page whose only moving part is the logo '''
    template_values={'logo': LOGO()}
    return JINJA_ENVIRONMENT.get_template(template_filename).render(template_values)


class MainPage(w.RequestHandler):
    def get(self):
        html = get_strmpage_html('index.html')
        self.response.write(html)	
class MainPageNoJS(w.RequestHandler):
    def get(self):
        html = get_strmpage_html('index_nojs.html')
        self.response.write(html)	
class MainPageNoJS_HTML5(w.RequestHandler):
    def get(self):
        html = get_strmpage_html('index_nojs_html5.html')
        self.response.write(html)	
class Lite(w.RequestHandler):
    def get(self):
        html = get_strmpage_html('lite.html')
        self.response.write(html)	

class StreamerList(w.RequestHandler):
    def get(self):
        twitchers = getTwitchersList()
        hitboxers = getHitboxersList()
        twitchers.sort()
        hitboxers.sort()
        template_values={
                        'twitchers': twitchers,
                        'hitboxers': hitboxers,
                        'logo': LOGO(),
                        }
        template = JINJA_ENVIRONMENT.get_template('strmlst.html')
        self.response.write(template.render(template_values))

class AddTwitcher(w.RequestHandler):
    def post(self):
        memcache.delete_multi(['twitchers', 'hitboxers']) #cache is out-of-date
        t=Twitcher()
        t.login = self.request.get('login')
        if t.login == "undefined":
            self.redirect('/not_logged_in')
        else:
            t.put()
            self.redirect('/thanks')
						
class RemoveTwitcher(w.RequestHandler):
    def post(self):
        memcache.delete_multi(['twitchers', 'hitboxers']) #cache is out-of-date
        username = self.request.get('username')
        if username == "undefined":
            self.redirect('/not_logged_in')
        else:
            query = db.GqlQuery("SELECT * FROM Twitcher WHERE login = '" + username + "'")
            for name in query:
                name.delete()
            self.redirect('/thanks')
                    
class AddHitboxer(w.RequestHandler):
    def post(self):
        memcache.delete_multi(['twitchers',  'hitboxers']) #cache is out-of-date
        h=Hitboxer()
        h.login = self.request.get('login')
        h.put()
        self.redirect('/thanks')

'''
class TwitchHider(w.RequestHandler):
    def post(self):
        #session = get_current_session()
        login = self.request.get('login')
        #hiddenTwitchers=session.get('hiddenTwitchers',[])
        hiddenTwitchers = cookieGetList(self.request, 'hiddenTwitchers')
        
        if login not in hiddenTwitchers:
                        hiddenTwitchers.append(login)
        #session['hiddenTwitchers']=hiddenTwitchers
        cookieSetList(self.response, 'hiddenTwitchers', hiddenTwitchers)
        
        self.redirect('/strmlst')
		
class TwitchUnHider(w.RequestHandler):
    def post(self):
        #session = get_current_session()
        login = self.request.get('login')
        #hiddenTwitchers=session.get('hiddenTwitchers',[])
        hiddenTwitchers = cookieGetList(self.request, 'hiddenTwitchers')
        
        if login in hiddenTwitchers:
                        hiddenTwitchers.remove(login)
        #session['hiddenTwitchers']=hiddenTwitchers
        cookieSetList(self.response, 'hiddenTwitchers', hiddenTwitchers)
        
        self.redirect('/strmlst')
'''

class List(w.RequestHandler):
    def get(self):
        self.response.write(json.dumps({ "twitchers" : getTwitchersList(),  "hitboxers":getHitboxersList()}))

class NotLoggedIn(w.RequestHandler):
    def get(self):
        html = get_logopage_html('not_logged_in.html')
        self.response.write(html)
class Todo(w.RequestHandler):
    def get(self):
        html = get_logopage_html('todo.html')
        self.response.write(html)
class Thanks(w.RequestHandler):
    def get(self):
        html = get_logopage_html('thanks.html')
        self.response.write(html)
class About(w.RequestHandler):
    def get(self):
        html = get_logopage_html('about.html')
        self.response.write(html)
class Pofv(w.RequestHandler):
    def get(self):
        html = get_logopage_html('pofv.html')
        self.response.write(html)
class Sukoata(w.RequestHandler):
    def get(self):
        html = get_logopage_html('sukoata.html')
        self.response.write(html)
class Sing(w.RequestHandler):
    def get(self):
        html = get_logopage_html('sing.html')
        self.response.write(html)
class Quota(w.RequestHandler):
    def get(self):
        html = get_logopage_html('quota.html')
        self.response.write(html)
class Maintenance(w.RequestHandler):
    def get(self):
        html = get_logopage_html('maintenance.html')
        self.response.write(html)
		
